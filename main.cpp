#include <QCoreApplication>
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QVector>
#include <QMatrix4x4>
#include <QVector3D>
#include <QMap>
#include <QStringList>
#include <QSet>
#include <iostream>

QString settingsfilename = "settings.txt";
QString dbpath, renderpath, taskdir;
QString dbmodelspath, rendermodelspath, rendertaskpath;

QVector<QString> sceneList; 
QString currScenename, currScenepath;
QVector<QString> currObjList;
QVector<QMatrix4x4> currObjTransformList;

void loadSettings()
{
	// settings
	QFile infile(settingsfilename);
	if (!infile.open(QIODevice::ReadOnly)) return;
	QTextStream instream(&infile);
	while (!instream.atEnd())
	{
		QString tag = instream.readLine();
		if (tag == "-dbpath") dbpath = instream.readLine();
		else if (tag == "-renderpath") renderpath = instream.readLine();
		else if (tag == "-taskdir") taskdir = instream.readLine();
		else continue;
	}
	infile.close();
	dbmodelspath = dbpath + "models_with_texture/";
	rendermodelspath = renderpath + "models_with_texture/";
	rendertaskpath = renderpath + taskdir;

	// scene list
	QDir dir(rendertaskpath);
	QFileInfoList fileInfoList = dir.entryInfoList();
	for (QFileInfo fileInfo : fileInfoList)
	{
		if (fileInfo.isFile()) continue;

		QString subdirpath = fileInfo.filePath();
		auto pos = subdirpath.lastIndexOf('/');
		QString scenename = subdirpath.right(subdirpath.length() - pos - 1);
		if (scenename == "." || scenename == "..") continue;

		sceneList << scenename;
	}
}

void loadCurrObjList()
{
	currObjList.clear();
	currObjTransformList.clear();

	QString txtfilename;
	QDir dir(currScenepath);
	QFileInfoList fileInfoList = dir.entryInfoList();
	for (QFileInfo fileInfo : fileInfoList)
	{
		if (fileInfo.isFile() && fileInfo.suffix() == "txt")
		{
			txtfilename = fileInfo.baseName();
			break;
		}
	}

	QFile infile(currScenepath + txtfilename + ".txt");
	if (!infile.open(QIODevice::ReadOnly)) return;
	QTextStream instream(&infile);

	while (!instream.atEnd())
	{
		QString tag; instream >> tag;
		if (tag == "newModel")
		{
			int mid; instream >> mid;
			QString objName; instream >> objName;

			// do not merge the room
			if (objName.contains("room")){
				instream.readLine(); // skip the transform
				continue;
			}

			currObjList << objName;

			instream >> tag;
			QMatrix4x4 T;
			for (int i = 0; i < 16; i++)
			{
				int r = i / 4;
				int c = i % 4;
				instream >> T(c, r);
			}
			currObjTransformList << T;
		}
	}
	infile.close();
}

void mergeMtlFiles()
{
	QFile outfile(currScenepath + currScenename + ".mtl");
	if (!outfile.open(QIODevice::WriteOnly)) return;
	QTextStream outstream(&outfile);

	// merge all mtl files into one
	for (QString objname : currObjList)
	{
		QFile infile(dbmodelspath + objname + ".mtl");
		if (!infile.open(QIODevice::ReadOnly)) return;
		QTextStream instream(&infile);

		outstream << "\n#### This is separator ####\n";
		// read line by line
		while (!instream.atEnd())
		{
			// read line and split
			QString line = instream.readLine();
			QStringList list = line.split(QRegExp("\\s+"));
			QString tag = list.first();
			if (tag == "newmtl")
			{
				// use mtlFilename as prefix
				QString mtlName = list.last();
				QString mtlName_new = objname + "_" + mtlName;

				outstream << "newmtl " << mtlName_new << "\n";
			}
			else if (tag.indexOf("map_") == 0)
			{
				QString imgname = rendermodelspath + list.last();
				list.removeLast(); list.push_back(imgname);
				for (QString str : list) { outstream << str << " "; }
				outstream << "\n";
			}
			else {
				// copy line
				outstream << line << "\n";
			}
		}

		infile.close();
	}

	outfile.close();
}

void mergeObjFiles()
{
	QFile outfile(currScenepath + currScenename + ".obj");
	if (!outfile.open(QIODevice::WriteOnly)) return;
	QTextStream outstream(&outfile);

	// use the scene mtl lib
	outstream << "mtllib " + currScenename + ".mtl\n";

	int vertexOffset = 0;
	// merge all obj files into one
	for (auto objId = 0; objId < currObjList.size(); objId++)
	{
		QString objname = currObjList[objId];
		QMatrix4x4 objTransform = currObjTransformList[objId];

		QFile infile(dbmodelspath + objname + ".obj");
		if (!infile.open(QIODevice::ReadOnly)) return;
		QTextStream instream(&infile);

		// wrap each obj as a single group
		outstream << "\n#### This is separator ####\n";
		outstream << "g " << objname << "\n";

		// read line by line
		int nV = 0;
		while (!instream.atEnd())
		{
			// read line and split
			QString line = instream.readLine();
			QStringList list = line.split(QRegExp("\\s+"));
			QString tag = list.first();
			if (tag == "mtllib" || tag == "g" || tag == "vn")
			{
				// skip mtl lib, and groups
				continue;
			}
	
			if (tag == "usemtl")
			{
				// use mtlFilename as prefix
				QString mtlName = list.last();
				QString mtlName_new = objname + "_" + mtlName;

				outstream << "usemtl " << mtlName_new << "\n";
			}
			else if (tag == "v")
			{
				// apply transformation
				QVector4D p;
				p[0] = list[1].toDouble();
				p[1] = list[2].toDouble();
				p[2] = list[3].toDouble();
				p[3] = 1.0;
				p = objTransform * p;
				outstream << "v " << p[0] << " " << p[1] << " " << p[2]<< "\n";
				//outstream << line << "\n";
				nV++;
			}
			else if (tag == "f")
			{
				outstream << tag;
				for (auto i = 1; i < 4; i++)
				{
					QString vvtvn = list[i];
					QStringList vprolist = vvtvn.split("/");
					int vid = vprolist.first().toInt() + vertexOffset;
					outstream << " " << vid;
				}
				outstream << "\n";
			}
			else
			{
				// copy
				outstream << line << "\n";
			}
			
		}

		// update vertex offset
		vertexOffset += nV;

		infile.close();
	}

	outfile.close();
}


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

	qDebug() << "Loading settings...";
	loadSettings();

	qDebug() << "Start merging...";
	
	// create merged files for each frame
	for (auto scenename : sceneList)
	{
		std::cout << scenename.toStdString() << "\t:";

		currScenename = scenename;
		currScenepath = rendertaskpath + scenename + "/";

		QDir dir(currScenepath);
		if (dir.exists(scenename + ".obj"))
		{
			std::cout << "+\n";
			continue;
		}

		loadCurrObjList();
		mergeMtlFiles();
		mergeObjFiles();

		std::cout << "Merged.\n";
	}

	qDebug() << "Done.";

    return a.exec();
}

